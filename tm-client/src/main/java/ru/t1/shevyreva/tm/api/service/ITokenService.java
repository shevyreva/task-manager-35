package ru.t1.shevyreva.tm.api.service;

import org.jetbrains.annotations.Nullable;

public interface ITokenService {

    public String getToken();

    public void setToken(@Nullable final String token);
}
