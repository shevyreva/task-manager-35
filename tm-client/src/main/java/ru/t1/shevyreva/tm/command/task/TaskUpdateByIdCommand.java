package ru.t1.shevyreva.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.request.task.TaskUpdateByIdRequest;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-update-by-id";

    @NotNull
    private final String DESCRIPTION = "Update task by Id.";

    @NotNull
    @Override
    public String getName() {
        return this.NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE BY ID]");
        System.out.println("Enter Id:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("Enter Name:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter Description:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(getToken());
        request.setId(id);
        request.setDescription(description);
        request.setName(name);
        getTaskEndpoint().updateTaskById(request);
    }

}
