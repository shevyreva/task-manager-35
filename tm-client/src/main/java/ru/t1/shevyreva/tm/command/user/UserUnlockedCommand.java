package ru.t1.shevyreva.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.request.user.UserUnlockRequest;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.util.TerminalUtil;

public class UserUnlockedCommand extends AbstractUserCommand {

    @NotNull
    private final String DESCRIPTION = "Unlock user.";

    @NotNull
    private final String NAME = "user-unlock";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[UNLOCK USER]");
        System.out.println("Enter user login:");
        @NotNull final String login = TerminalUtil.nextLine();
        @NotNull final UserUnlockRequest request = new UserUnlockRequest();
        request.setLogin(login);
        getUserEndpoint().unlockUser(request);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
