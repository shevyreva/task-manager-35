package ru.t1.shevyreva.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.request.AbstractUserRequest;
import ru.t1.shevyreva.tm.enumerated.ProjectSort;

@Getter
@Setter
@NoArgsConstructor
public class ProjectListRequest extends AbstractUserRequest {

    @Nullable
    private ProjectSort sort;

    public ProjectListRequest(@Nullable ProjectSort sort) {
        this.sort = sort;
    }

    public ProjectListRequest(@Nullable String token) {
        super(token);
    }

}
