package ru.t1.shevyreva.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class UserRemoveRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    @Nullable
    private String email;

    @Nullable
    private String userId;

    public UserRemoveRequest(
            @Nullable final String login,
            @Nullable final String email,
            @Nullable final String id) {
        this.login = login;
        this.email = email;
        this.userId = id;
    }

}
