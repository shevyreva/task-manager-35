package ru.t1.shevyreva.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.response.AbstractResponse;
import ru.t1.shevyreva.tm.model.Project;

@Getter
@NoArgsConstructor
public abstract class AbstractProjectResponse extends AbstractResponse {

    @Nullable
    private Project project;

    public AbstractProjectResponse(@Nullable Project project) {
        this.project = project;
    }

}
