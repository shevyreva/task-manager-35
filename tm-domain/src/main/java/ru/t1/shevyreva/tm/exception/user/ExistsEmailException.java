package ru.t1.shevyreva.tm.exception.user;

public class ExistsEmailException extends AbstractUserException {

    public ExistsEmailException() {
        super("Error! Email has already existed.");
    }

}
