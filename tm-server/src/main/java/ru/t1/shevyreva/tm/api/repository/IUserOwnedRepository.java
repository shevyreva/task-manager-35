package ru.t1.shevyreva.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    @NotNull
    M add(String userId, M model);

    @NotNull
    List<M> findAll(String userId);

    @NotNull
    List<M> findAll(String userId, Comparator<M> comparator);

    void removeAll(String userId);

    @Nullable
    M findOneById(String userId, String id);

    @Nullable
    M findOneByIndex(String userId, Integer index);

    @NotNull
    M removeOne(String userId, M model);

    @NotNull
    M removeOneById(String userId, String id);

    @Nullable
    M removeOneByIndex(String userId, Integer index);

    boolean existsById(String userId, String id);

    @NotNull
    int getSize(String userId);

}
