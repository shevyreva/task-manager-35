package ru.t1.shevyreva.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.repository.IUserOwnedRepository;
import ru.t1.shevyreva.tm.exception.entity.ModelNotFoundException;
import ru.t1.shevyreva.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M>
        implements IUserOwnedRepository<M> {

    @NotNull
    @Override
    public M add(@NotNull final String userId, @NotNull final M model) {
        model.setUserId(userId);
        records.add(model);
        return model;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId) {
        return records.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId, @NotNull final Comparator<M> comparator) {
        final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String userId, @NotNull final String id) {
        return records.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .filter(m -> id.equals(m.getId()))
                .findFirst().orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return records.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .skip(index)
                .findFirst().orElse(null);
    }

    public void removeAll(@NotNull final String userId) {
        @NotNull final List<M> models = findAll(userId);
        removeAll(models);
    }

    public void removeAll(final List<M> models) {
        for (M model : models)
            this.removeOne(model);
    }

    @NotNull
    public M removeOne(@NotNull final String userId, @NotNull final M model) {
        return removeOneById(userId, model.getId());
    }

    @NotNull
    public M removeOne(@NotNull final M model) {
        records.remove(model);
        return model;
    }

    @NotNull
    @Override
    public M removeOneById(final String userId, final String id) {
        @Nullable final M model = findOneById(userId, id);
        if (model == null) throw new ModelNotFoundException();
        return this.removeOne(model);
    }

    @NotNull
    @Override
    public M removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) throw new ModelNotFoundException();
        return this.removeOne(model);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final M model = findOneById(userId, id);
        return model != null;
    }

    @NotNull
    @Override
    public int getSize(@NotNull final String userId) {
        return (int) records.stream()
                .filter(m -> userId.equals(m.getUserId()))
                .count();
    }

}
